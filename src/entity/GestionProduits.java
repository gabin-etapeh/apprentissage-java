package entity;

import java.util.ArrayList;
import java.util.List;

// Definition d'une classe simple
class ProduitDto {
    private String nomDto;
    private Integer quantiteDto;
    private Double prixDto;

    public ProduitDto() {
    }

    public ProduitDto(String nomDto, Integer quantiteDto, Double prixDto) {
        this.nomDto = nomDto;
        this.quantiteDto = quantiteDto;
        this.prixDto = prixDto;
    }

    public String getNomDto() {
        return nomDto;
    }

    public Integer getQuantiteDto() {
        return quantiteDto;
    }

    public Double getPrixDto() {
        return prixDto;
    }

    public void setNomDto(String nomDto) {
        this.nomDto = nomDto;
    }

    public void setQuantiteDto(Integer quantiteDto) {
        this.quantiteDto = quantiteDto;
    }

    public void setPrixDto(Double prixDto) {
        this.prixDto = prixDto;
    }
}

class Produit {

    private Integer id;
    private String nom;
    private Integer quantite;
    private Double prix;

    public Produit() {
    }

    public Produit(Integer id, String nom, Integer quantite, Double prix) {
        this.id = id;
        this.nom = nom;
        this.quantite = quantite;
        this.prix = prix;
    }

    public Integer getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public Double getPrix() {
        return prix;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }
}

public class GestionProduits {

    public static void main(String[] args) {

        // 1. extraire tout le code suivant en une méthode: createProductDtos()
        List<ProduitDto> produitDtos = createProductDtos();

        // 2. Extraire le code suivant en une méthodes appelée createProducts() qui va utiliser le résulats
        //  de createProductsDto()
        List<Produit> produits = createProducts(produitDtos);

        // 3. Extraire ce code en une méthode appelée computeTotalProducts()
        computeTotalProducts(produits);

        // 4. Extraire le code suivant en une méthode appelée computeMinMaxProducts()
        computeMinMaxProducts(produits);

        // 5. Extraire le code suivant en une fonction appelée printProducts()
        printProducts(produits);
    }

    public static List<ProduitDto> createProductDtos(){
        ProduitDto firstProduitDto = new ProduitDto("savon",2,600.0);
        ProduitDto secondProduitDto = new ProduitDto("cube",3,800.0);
        ProduitDto thirdProduitDto = new ProduitDto("stylos",5,400.0);
        ProduitDto fourthProduitDto = new ProduitDto("craie",10,5600.0);
        ProduitDto fifthProduitDto = new ProduitDto("crayon",12,900.0);

        return new ArrayList<>(List.of(firstProduitDto, secondProduitDto, thirdProduitDto, fourthProduitDto, fifthProduitDto));
    }

    public static List<Produit> createProducts (List<ProduitDto> produitDtos){
        List<Produit> resultats = new ArrayList<>();
        int id = 1;
        for (ProduitDto produitDto : produitDtos){
            Produit produit = new Produit(id++,
                    produitDto.getNomDto(),
                    produitDto.getQuantiteDto(),
                    produitDto.getPrixDto());
            resultats.add(produit);
        }
        return resultats;
    }

    private static void computeTotalProducts(List<Produit> produits) {
        Integer total = 0;
        for (Produit produit : produits){
            total = total + produit.getQuantite();
        }
        System.out.println("Le total des produits est: " + total);
    }

    private static void computeMinMaxProducts(List<Produit> produits) {
        Double prixMaximal = produits.get(0).getPrix();
        Double prixMinimal = produits.get(0).getPrix();
        for (Produit produit : produits){

            Double prixCourant = produit.getPrix();

            if(prixMaximal < prixCourant){
                prixMaximal = prixCourant;
            }

            if(prixMinimal > prixCourant){
                prixMinimal = prixCourant;
            }
        }

        System.out.println("Le prix maximum est: " + prixMaximal + " et le prix minimum est :" + prixMinimal);
    }

    public static void printProducts(List<Produit> produits){
        for (Produit produit : produits){
            System.out.println(produit.getId() + " " + produit.getNom()+ " " + produit.getQuantite() + " " + produit.getPrix());
        }
    }
}
